#!/bin/bash
# This script import the XFCE configuration

git clone https://gitlab.com/myscripts12/myxfce.git /tmp/myxfce
GIT_REPO=/tmp/myxfce
USER=$(users)
GROUP=$USER

# Check if the desktop environment corresponds to XFCE, if not exit with error
[ $XDG_CURRENT_DESKTOP != "XFCE" ] && ( printf "OS not supported\n" ; exit 1 )

printf "\n########## Install programs ##########\n"
paru -S --noconfirm --quiet compiz xfce4-cpufreq-plugin

printf "\n########## Save old configuration folders ########\n"
rm -rf /tmp/xfce_config/ 2> /dev/null
mkdir /tmp/xfce_config
cp -r ~/.config/{xfce4,autostart,Thunar,compiz-1} /tmp/xfce_config

printf "\n########## Copy new config files ##########\n"
cp -r $GIT_REPO/config/{xfce4,autostart,Thunar,compiz-1} ~/.config/
sudo chown -R $USER:$GROUP ~/.config/{xfce4,autostart,Thunar,compiz-1}

printf "\n########## Cleaning ##########\n"
rm -rf /tmp/myxfce

printf "\n########## Reboot ##########\n"
sudo reboot now
