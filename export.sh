#!/bin/bash
# This script export the XFCE4 configuration

GIT_REPO=$(pwd)


printf "\n########## Remove old config folder ########\n"
rm -rf $GIT_REPO/config
mkdir $GIT_REPO/config

printf "\n########## Copy new config files ##########\n"
cp -r ~/.config/{xfce4,autostart,Thunar,compiz-1} $GIT_REPO/config/
cp ~/.config/{gesturesmanger.json,libinput-gestures.conf} $GIT_REPO/config/

printf "\n########## Git push ##########\n"
git add config/*
git rm -r config/*
git commit -m "Update xfce4, autostart and Thunar"
git push origin master
