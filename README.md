# myXFCE

This repository contains my XFCE desktop configuration. There is an import an export script. The import script will import the current configuration while the export script will export the configuration to this repo.

## Supported OS

Currently the following OSes are supported:

- [x] Centos 8
- [x] Manjaro
- [x] Ubuntu 20.04
- [x] Debian
- [x] ArchLinux

## Prerequisites

The base packages have to installed in order to this script to work properly. If not it may break the import.

## Import

The import script will import the current XFCE. It has to be run in a TTY and not in the desktop environment!

```zsh
curl https://gitlab.com/myscripts12/myxfce/-/raw/master/import.sh | bash
```

## Export

The export script will export the current XFCE configuration to this repo.

```zsh
curl https://gitlab.com/myscripts12/myxfce/-/raw/master/export.sh | bash
```
